# Meera New - traditional orthography opentype Unicode font for Malayalam #

Meera New is a complete redesign of Meera font which was distributed since 2008.
Major modification done is reducing width of characters, i.e., condensing,
thus answering a long time criticism on original Meera that it took more space
compared to Rachana. Vertical conjuncts are designed with a new proportion
between above and below characters for better leading and inking.

Meera New is a sans serif font following the limited character set of Malayalam
traditional orthography. It is suitable for short texts like box texts, blurbs
and section headings.

## License ##
Meera New is licensed under Open Font License 1.1.

## Authors ##
Hussain KH (typography), Rajeesh KV (font engineering),
Rachana Institute of Typography (http://rachana.org.in).

## Colophon ##
This font is an offspring of Rachana movement under the leadership of 
R. Chitrajakumar who founded Rachana Akshara Vedi in 1999. He devised the
‘definitive character set’ of Malayalam based on traditional script which is
the base of Rachana as well as this font.

The naming convention of glyphs (`k1` for `ക`, `k2` for `ഖ` etc.) is devised
by K.H. Hussain in 2005 to form a mnemonic way to represent conjuncts and its
components and to facilitate the coding of glyph substitution of conjuncts.
